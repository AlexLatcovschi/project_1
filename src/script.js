var headerHeight;

function addItemActive() {
    const items = document.querySelectorAll('.header .menu-items .item')
    items.forEach(el => {
        el.addEventListener('click', () => {
            console.log('el: ', el.classList);
            items.forEach(nestedItem => {
                nestedItem.classList.remove('active');
            })
            el.classList.add('active');
        })
    })
}

window.onload = () => {
    addItemActive();
    headerHeight = document.querySelectorAll('.header')[0].offsetHeight;
}

window.addEventListener('scroll', (e) => {
    console.log('window.scrollY: ', window.scrollY, headerHeight);
    if (window.scrollY > headerHeight) {
        document.querySelectorAll('.header')[0].classList.add('fixed');
    } else {
        document.querySelectorAll('.header')[0].classList.remove('fixed');
    }
});
